## install pyearth (MARS)
rm -Rf py-earth
git clone https://github.com/jcrudy/py-earth.git

cd py-earth  
pipenv run $(pipenv --venv)/bin/python setup.py build_ext --inplace --cythonize

mv pyearth $(pipenv --venv)/lib/python3.9/site-packages
cd ../ && rm -Rf py-earth 

echo "pyearth installed successfully to pipenv"
